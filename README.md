This is a Google Chrome plugin for anyone with UPMC account to (legally) download research papers from outside UPMC network.

Obviously, UPMC doesn't subscribe to all academic publications in the world. This plugin, unfortunately, can't tell whether UPMC has the subscription to a particular paper. What this plugin does is redirecting you to a special UPMC URL with the paper URL as a GET parameter (http://accesdistant.upmc.fr/login?url=the_paper_URL). Then, the UPMC server will ask for your credential. If UPMC has the subscription, you will be redirected to the URL of the "full-version" paper; otherwise where you will end up is up to UPMC server :p

images/book_original.png is from http://publicicons.org/book-icon/ . images/book_small.png is the resized version. This image is used as the plugin's icon.

I got the remote access URL from http://bupmc.ent.upmc.fr/fr/ressources_en_ligne2/mode_acces_ressources.html (the page is in French).

To install this plugin:
1. In your chrome, open "chrome://extensions"

2. Turn on the developer mode

3. Click "Load unpacked"

4. Select the directory of this plugin

5. It's done!
