'use strict';

document.getElementById('accessor').addEventListener('click', function(element) {
  chrome.tabs.query({
              active: true,
              lastFocusedWindow: true
          }, function(tabs) {
              if (tabs.length == 0) {
                return;
              }
              var tab = tabs[0];
              var newUrl = "http://accesdistant.upmc.fr/login?url="+encodeURI(tab.url);
              window.open(newUrl);
          });
});
